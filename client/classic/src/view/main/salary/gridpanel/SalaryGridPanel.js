Ext.define('IncomeCalculator.view.main.salary.gridpanel.SalaryGridPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'salarygridpanel',

    requires: [
        'IncomeCalculator.I18n',
        'IncomeCalculator.view.main.salary.gridpanel.grid.SalaryGrid',
        'IncomeCalculator.view.main.salary.gridpanel.GridPanelController'
    ],

    controller: 'gridpanel',
    layout: 'fit',

    items: [{
        layout: {
            type:'vbox',
            align: 'left',
            padding: 10
        },
        items: [{
            xtype: 'salarygrid',
            reference: 'salarygrid',
            width: '100%',
            region: 'center',
            flex: 3
        },
        {
            xtype: 'button',
            text: I18n.get('salarygrid.button.add'),
            width: 100,
            margin: '10 0',
            flex: 1,
            listeners: {
                click: 'onAddSalary'
            }
        }]
    }]
});