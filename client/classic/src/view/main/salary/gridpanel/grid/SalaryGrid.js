Ext.define('IncomeCalculator.view.main.salary.gridpanel.grid.SalaryGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'salarygrid',

    requires: [
        'IncomeCalculator.I18n',
        'IncomeCalculator.store.SalaryStore',
        'IncomeCalculator.view.main.salary.gridpanel.grid.GridController',
        'IncomeCalculator.view.main.salary.gridpanel.components.countrycombobox.CountryStore',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.state.*',
        'Ext.form.*',
        'Ext.panel.*'
    ],

    plugins: {
            ptype: 'cellediting',
            clicksToEdit: 1
    },

    buttonAlign : 'start',

    title: I18n.get('salarygrid.title'),

    store: {
        type: 'salarystore'
    },

    controller: 'grid',

    layout: 'fit',

    columns: [
        {
           text: I18n.get('salarygrid.column.id'),
           dataIndex:'id',
           type:'number',
           flex:1
        },
        {
           text:I18n.get('salarygrid.column.dailyNetIncome'),
           dataIndex:'dailyNetIncome',
           type:'numbercolumn',
           flex:3,
           editor: {
               xtype: 'numberfield',
               allowBlank: false,
               minValue: 0
           }
        },
        {
           text:I18n.get('salarygrid.column.countryCode'),
           dataIndex:'countryCode',
           type:'gridcolumn',
           flex:1,
           editor: {
                xtype : 'combobox',
                id : 'countryCodeCombobox',
                store : Ext.create('IncomeCalculator.view.main.salary.gridpanel.components.countrycombobox.CountryStore'),
                displayField : 'countryCode',
                valueField : 'countryCode',
                editable: false,
                mode : 'local'
           }
        },
        {
           text:I18n.get('salarygrid.column.currencyCode'),
           dataIndex:'currencyCode',
           type:'string',
           flex:1
        },
        {
           text:I18n.get('salarygrid.column.tax'),
           dataIndex:'tax',
           type:'number',
           flex:1,
           renderer: 'percentRenderer'
        },
        {
           text:I18n.get('salarygrid.column.fixedExpense'),
           dataIndex:'fixedExpense',
           type:'number',
           flex:2,
           renderer: 'numberRenderer'
        },
        {
           text:I18n.get('salarygrid.column.monthlyNetSalary'),
           dataIndex:'monthlyNetSalary',
           type:'number',
           flex:3,
           renderer: Ext.util.Format.numberRenderer('0.00'),
           cls: 'bold-field'
        }
    ]

});
