Ext.define('IncomeCalculator.view.main.salary.gridpanel.components.countrycombobox.CountryStore', {
    extend: 'Ext.data.Store',

    alias: 'store.country',

    model: 'IncomeCalculator.model.CountryModel',

    proxy: {
         type: 'ajax',
         url: '/currencies',
         reader: {
             type: 'json'
         }
    },
    autoLoad: true

});
