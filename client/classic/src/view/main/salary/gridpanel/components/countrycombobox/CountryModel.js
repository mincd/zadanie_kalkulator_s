Ext.define('IncomeCalculator.model.CountryModel', {
    extend: 'IncomeCalculator.model.Base',

    fields: [
        {
            name: 'countryCode',
            type: 'string'
        }
    ]
});
