Ext.define('IncomeCalculator.view.main.salary.SalaryPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'salarypanel',

    requires: [
        'IncomeCalculator.view.main.salary.gridpanel.SalaryGridPanel',
        'IncomeCalculator.view.main.salary.footer.SalaryFooter'

    ],

    id: 'salarypanel',
    layout: {
        type:'vbox',
        align: 'stretch',
        padding: 10
    },

    items: [
    {
        xtype: 'salarygridpanel',
        flex: 3
    },
    {
        xtype: 'salaryfooter',
        flex: 1
    }]

});