Ext.define('IncomeCalculator.view.main.salary.footer.SalaryFooter', {
    extend: 'Ext.panel.Panel',
    xtype: 'salaryfooter',

    controller: 'footer',

    requires: [
        'IncomeCalculator.I18n',
        'Ext.button.*',
        'IncomeCalculator.view.main.salary.footer.FooterController'
    ],

    layout: {
        type:'hbox',
        align: 'begin',
        pack: 'center',
        padding: 10
    },

    items: [{
        xtype: 'button',
        text : I18n.get('salarygrid.button.update'),
        listeners: {
            click: 'onUpdateClick'
        }
    }]

});