/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('IncomeCalculator.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: I18n.get('app.name')
    }

});
