/**
 * This is the controller for the salary grid.
 */
Ext.define('IncomeCalculator.view.main.salary.gridpanel.grid.GridController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.grid',

    requires: [
        'IncomeCalculator.store.SalaryStore'
    ],

    config: {
        stores: ['salarystore']
    },

    /**
     * renders zero as empty string
     */
    numberRenderer: function(value) {
       if(value == 0) {
         return '';
       }
       return value;
    },

    /**
     * renders float field and adds percent sign
     */
    percentRenderer: function(value) {
       if(value == 0) {
         return '';
       }
       return '' + value * 100 + '%';
    }
});
