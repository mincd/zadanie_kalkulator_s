/**
 * This is the controller for the grid panel
 */
Ext.define('IncomeCalculator.view.main.salary.gridpanel.GridPanelController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.gridpanel',

    SALARY_GRID_ELEMENT: 'salarygrid',

    /**
    * Add salary button
    */
    onAddSalary: function (button) {
        var grid = this.getView().down(this.SALARY_GRID_ELEMENT);
        var store = grid.getStore();
        store.addEmptySalary();
    }
});
