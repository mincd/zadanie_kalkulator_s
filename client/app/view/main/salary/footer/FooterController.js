/**
 * This is the controller for the grid panel
 */
Ext.define('IncomeCalculator.view.main.salary.footer.FooterController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.footer',

    /**
    * Update click listener
    */
    onUpdateClick: function(button) {
        var me = this;
        var loadingMask = new Ext.LoadMask({
            msg    : I18n.get('app.loading'),
            target : Ext.getCmp('salarypanel')
        });
        loadingMask.show();
        var salaryStore = Ext.StoreManager.lookup('salarystore');
        var data = salaryStore.getData().items.map(function (record) {
            return {
                'id' : record.getData().id,
                'dailyNetIncome' : record.getData().dailyNetIncome,
                'countryCode' : record.getData().countryCode
            };
        });
        Ext.Ajax.request({
            url: '/calculate-income',
            method: 'POST',
            jsonData: data,
            scope: this,
            success: function(response, opts) {
                var json = Ext.decode(response.responseText);
                me.updateSalaryStore(json);
                loadingMask.hide();
            },

            failure: function(response, opts) {
                loadingMask.hide();
                if (response.timedout) {
                    Ext.Msg.alert(I18n.get('salarygrid.error.update'), I18n.get('salarygrid.error.timedout'),
                                        Ext.emptyFn);
                } else {
                    var parsedResponse = Ext.decode(response.responseText);
                    Ext.Msg.alert(I18n.get('salarygrid.error.update'), parsedResponse.message,
                        Ext.emptyFn);
                }
            }
        });
    },
    /**
    * Update salary store with data from server
    */
    updateSalaryStore: function(json) {
        json.forEach(this.updateSalary);
    },

    /**
    * Update salary store record with jsonRecord from server
    */
    updateSalary: function(jsonRecord, index, ar) {
        var salaryStore = Ext.StoreManager.lookup('salarystore');
        salaryStore.updateSalary(jsonRecord);
    }
});
