Ext.define('IncomeCalculator.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'IncomeCalculator.model'
    }
});
