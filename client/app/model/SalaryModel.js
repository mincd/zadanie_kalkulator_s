Ext.define('IncomeCalculator.model.SalaryModel', {
    extend: 'IncomeCalculator.model.Base',

    fields: [
        {name: 'id',  type: 'number'},
        {name: 'dailyNetIncome',  type: 'number', minValue: 0},
        {name: 'countryCode',  type: 'string'},
        {name: 'currencyCode',  type: 'string'},
        {name: 'tax',  type: 'number'},
        {name: 'fixedExpense',  type: 'number'},
        {name: 'monthlyNetSalary',  type: 'number'},
    ]
});
