/**
* Store for salaries. This store usage could be changed to use the proxy
* and send the whole record to the server - it would easier the store update,
* but it will increase the amount of sent data (unneeded data)
*/
Ext.define('IncomeCalculator.store.SalaryStore', {
    extend: 'Ext.data.Store',

    alias: 'store.salarystore',

    requires: [
        'Ext.window.MessageBox',
        'IncomeCalculator.I18n'
    ],

    model: 'IncomeCalculator.model.SalaryModel',

    storeId: 'salarystore',

    data: { items: [
        { id: 1, dailyNetIncome: 0, countryCode: "PL" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

    /**
    * Add new salary record
    */
    addEmptySalary: function() {
        if (this.data.length < 10){
            this.add({ id: this.data.length + 1, dailyNetIncome: 0, countryCode: "PL", currencyCode: "",  tax: null, fixedExpense: null, monthlyNetSalary: null});
        } else {
            Ext.Msg.alert('', I18n.get('salarygrid.error.maxSalaries'), Ext.emptyFn);
        }
    },

    /**
    * Update the salary in store based on the json data
    */
    updateSalary: function(jsonRecord) {
        var recordIndex = this.findBy(function(record,id) {
            return (record.get('id') == jsonRecord.id);
        });
        var storeRecord = this.getAt(recordIndex);

        storeRecord.set('currencyCode', jsonRecord.currencyCode);
        storeRecord.set('tax', jsonRecord.tax);
        storeRecord.set('fixedExpense', jsonRecord.fixedExpense);
        storeRecord.set('monthlyNetSalary', jsonRecord.monthlyNetSalary);

        this.commitChanges();
    }
});
