/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'IncomeCalculator.Application',

    name: 'IncomeCalculator',

    requires: [
        // This will automatically load all classes in the IncomeCalculator namespace
        // so that application classes do not need to require each other.
        'IncomeCalculator.*'
    ],

    // The name of the initial view to create.
    mainView: 'IncomeCalculator.view.main.Main'
});
