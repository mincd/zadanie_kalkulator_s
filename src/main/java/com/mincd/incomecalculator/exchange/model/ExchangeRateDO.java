package com.mincd.incomecalculator.exchange.model;

import java.util.Date;

/**
 * Sub-element of ExchangeCurrencyDO JSON object
 */
public class ExchangeRateDO {
    String no;
    Date effectiveDate;
    Double mid;

    public ExchangeRateDO() {
        //default constructor
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Double getMid() {
        return mid;
    }

    public void setMid(Double mid) {
        this.mid = mid;
    }
}
