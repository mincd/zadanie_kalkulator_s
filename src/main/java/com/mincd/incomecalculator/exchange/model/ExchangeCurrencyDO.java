package com.mincd.incomecalculator.exchange.model;

import java.util.List;

public class ExchangeCurrencyDO {

    String table;
    String currency;
    String code;
    List<ExchangeRateDO> rates;

    public ExchangeCurrencyDO() {
        //default constructor
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ExchangeRateDO> getRates() {
        return rates;
    }

    public void setRates(List<ExchangeRateDO> rates) {
        this.rates = rates;
    }
}
