package com.mincd.incomecalculator.exchange.service;

import com.mincd.incomecalculator.exchange.config.CachedCurrencies;
import com.mincd.incomecalculator.exchange.model.ExchangeCurrencyDO;
import com.mincd.incomecalculator.exchange.model.ExchangeRateDO;
import com.mincd.incomecalculator.general.exceptions.UnsupportedCurrencyException;
import com.mincd.incomecalculator.salary.model.CountryDO;
import com.mincd.incomecalculator.salary.model.CurrencyDO;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExchangeService {
    public static final String NBP_EXCHANGE_URI = "http://api.nbp.pl/api/exchangerates/rates/a/";
    private static final int UPDATE_INTERVAL_MINUTES = 5;
    private static final Logger logger = LoggerFactory.getLogger(ExchangeService.class);

    private Date nextUpdate;
    private boolean currentlyUpdating;

    @Autowired
    private CachedCurrencies cachedCurrencies;

    public ExchangeService() {
        this.nextUpdate = new Date(0);
        this.currentlyUpdating = false;
    }

    /**
     * Get country codes available in the application
     *
     * @return
     */
    public List<CountryDO> getAvailableCountries() {
        return cachedCurrencies.getCurrencies().stream()
                .map(c -> new CountryDO(c.getCountryCode()))
                .collect(Collectors.toList());
    }

    /**
     * Gets the exchange rate by country
     *
     * @param countryCode
     * @return
     * @throws UnsupportedCurrencyException
     */
    public CurrencyDO getExchangeRateByCountry(String countryCode) throws UnsupportedCurrencyException {
        CurrencyDO result;
        Optional<CurrencyDO> currency = cachedCurrencies.getCurrencies().stream()
                .filter(c -> c.getCountryCode().equals(countryCode))
                .findFirst();
        if (!currency.isPresent()) {
            String logMessage = String.format("Unsupported country: %s", countryCode);
            logger.warn(logMessage);
            throw new UnsupportedCurrencyException(logMessage);
        }
        result = currency.get();
        if (shouldUpdateRates()) {
            result = getExchangeRate(result.getCurrencyCode());
        }
        return result;
    }

    /**
     * Update the currencies rates if needed and get the currency from cache
     *
     * @param currencyCode
     * @return
     * @throws UnsupportedCurrencyException
     */
    public CurrencyDO getExchangeRate(String currencyCode) throws UnsupportedCurrencyException {

        checkRatesUpdates();

        Optional<CurrencyDO> currency = cachedCurrencies.getCurrencies().stream()
                .filter(c -> c.getCurrencyCode().equals(currencyCode))
                .findFirst();
        if (currency.isPresent()) {
            return currency.get();
        } else {
            String logMessage = String.format("Unsupported currency: %s", currencyCode);
            logger.warn(logMessage);
            throw new UnsupportedCurrencyException(String.format("Currency %s is not supported", currencyCode));
        }
    }

    /**
     * Check if exchange rates need update
     */
    private void checkRatesUpdates() {
        if (shouldUpdateRates()) {
            updateExchangeRates();
        }
    }

    private boolean shouldUpdateRates() {
        return !currentlyUpdating && nextUpdate.before(new Date());
    }

    /**
     * Update the cached currencies with values from exchange
     */
    private void updateExchangeRates() {
        this.currentlyUpdating = true;
        boolean allCurrenciesUpdated = true;
        for (CurrencyDO currency : cachedCurrencies.getCurrencies()) {
            if (currency.getCurrencyCode().equals(CachedCurrencies.BASE_CURRENCY_CODE)) {
                continue;
            }
            OptionalDouble rate = getExternalExchangeRate(currency.getCurrencyCode());
            if (rate.isPresent()) {
                currency.setExchangeRate(rate.getAsDouble());
            } else {
                allCurrenciesUpdated = false;
            }
        }
        if (allCurrenciesUpdated) {
            nextUpdate = DateUtils.addMinutes(new Date(), UPDATE_INTERVAL_MINUTES);
        }
        this.currentlyUpdating = false;
    }

    /**
     * External Exchange rest call
     *
     * @param currencyCode
     * @return
     * @throws RestClientException
     */
    private OptionalDouble getExternalExchangeRate(String currencyCode) {
        final String uri = new StringBuilder(NBP_EXCHANGE_URI)
                .append(currencyCode).append("?format=json").toString();
        OptionalDouble result = OptionalDouble.empty();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        try {
            ResponseEntity<ExchangeCurrencyDO> response = restTemplate.exchange(uri, HttpMethod.GET, entity, ExchangeCurrencyDO.class);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                result = parseRate(response);
            }
        } catch (RestClientException e) {
            logger.warn(e.getMessage());
        }

        return result;
    }

    /**
     * Get the newest, but valid (effective date is before current date) rate value
     *
     * @param response
     * @return
     */
    private OptionalDouble parseRate(ResponseEntity<ExchangeCurrencyDO> response) {
        List<ExchangeRateDO> rates = response.getBody().getRates();
        Optional<ExchangeRateDO> newestValidRate = Optional.empty();
        OptionalDouble result = OptionalDouble.empty();
        if (!rates.isEmpty()) {
            Comparator<ExchangeRateDO> byEffectiveDateDescending = Collections.reverseOrder(Comparator.comparing(ExchangeRateDO::getEffectiveDate));
            newestValidRate = rates.stream()
                    .filter(r -> r.getEffectiveDate().before(new Date()))
                    .sorted(byEffectiveDateDescending)
                    .findFirst();
        }
        if (newestValidRate.isPresent()) {
            result = OptionalDouble.of(newestValidRate.get().getMid());
        }
        return result;
    }

}
