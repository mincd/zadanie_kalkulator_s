package com.mincd.incomecalculator.exchange.config;

import com.mincd.incomecalculator.salary.model.CurrencyDO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration of supported currencies
 */
@Service
public class CachedCurrencies {

    public static final String BASE_CURRENCY_CODE = "PLN";

    private List<CurrencyDO> currencies;

    /**
     * Creates a list of supported currencies
     */
    public CachedCurrencies() {
        //Possible tweak: read it from eg. CSV file
        this.currencies = new ArrayList<>();
        this.currencies.add(new CurrencyDO(BASE_CURRENCY_CODE, "PL", 0.19f, 1200, 1));
        this.currencies.add(new CurrencyDO("GBP", "UK", 0.25f, 600, 0));
        this.currencies.add(new CurrencyDO("EUR", "DE", 0.2f, 800, 0));
    }

    public List<CurrencyDO> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<CurrencyDO> currencies) {
        this.currencies = currencies;
    }


}
