package com.mincd.incomecalculator.salary.controller;

import com.mincd.incomecalculator.general.exceptions.UnsupportedCurrencyException;
import com.mincd.incomecalculator.salary.model.DailySalaryDO;
import com.mincd.incomecalculator.salary.model.SalaryDO;
import com.mincd.incomecalculator.salary.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Controller used for salary calculations
 */
@Controller
public class SalaryController {

    @Autowired
    SalaryService salaryService;

    @RequestMapping(value = "/calculate-income", method = RequestMethod.POST)
    @ResponseBody
    List<SalaryDO> calculateIncome(@RequestBody List<DailySalaryDO> dailySalaryDOs) throws UnsupportedCurrencyException {
        return salaryService.calculateIncome(dailySalaryDOs);
    }

}
