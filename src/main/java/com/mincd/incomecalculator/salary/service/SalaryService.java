package com.mincd.incomecalculator.salary.service;

import com.mincd.incomecalculator.exchange.service.ExchangeService;
import com.mincd.incomecalculator.general.exceptions.UnsupportedCurrencyException;
import com.mincd.incomecalculator.general.utils.CurrencyUtils;
import com.mincd.incomecalculator.salary.model.CurrencyDO;
import com.mincd.incomecalculator.salary.model.DailySalaryDO;
import com.mincd.incomecalculator.salary.model.SalaryDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalaryService {

    private static final int WORKING_DAYS_PER_MONTH = 22;

    @Autowired
    private ExchangeService exchangeService;

    public List<SalaryDO> calculateIncome(List<DailySalaryDO> dailySalaryDOs) throws UnsupportedCurrencyException {
        List<SalaryDO> list = new ArrayList<>();
        for (DailySalaryDO dailySalaryDO : dailySalaryDOs) {
            SalaryDO salaryDO = calculateCountryIncome(dailySalaryDO);
            list.add(salaryDO);
        }
        return list;
    }

    private SalaryDO calculateCountryIncome(DailySalaryDO dailySalaryDO) throws UnsupportedCurrencyException {
        CurrencyDO currencyDO = exchangeService.getExchangeRateByCountry(dailySalaryDO.getCountryCode());
        Double monthlyNetSalary = dailySalaryDO.getDailyNetIncome() * WORKING_DAYS_PER_MONTH
                * (1f - currencyDO.getTax()) - currencyDO.getFixedExpense();
        return new SalaryDO(currencyDO, dailySalaryDO.getId(), CurrencyUtils.roundToTwoDecimals(monthlyNetSalary));
    }

}
