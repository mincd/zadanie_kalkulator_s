package com.mincd.incomecalculator.salary.model;

/**
 * Domain model for daily salary
 */
public class DailySalaryDO {
    private int id;
    private Double dailyNetIncome;
    private String countryCode;

    public DailySalaryDO() {
        //default contructor
    }

    public DailySalaryDO(int id, Double dailyNetIncome, String countryCode) {
        this.id = id;
        this.dailyNetIncome = dailyNetIncome;
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getDailyNetIncome() {
        return dailyNetIncome;
    }

    public void setDailyNetIncome(Double dailyNetIncome) {
        this.dailyNetIncome = dailyNetIncome;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
