package com.mincd.incomecalculator.salary.model;

/**
 * Domain model containing calculated salary
 */
public class SalaryDO extends CurrencyDO {
    private int id;
    private double monthlyNetSalary;

    public SalaryDO() {
        //default constructor
    }

    public SalaryDO(int id, String currencyCode, float tax, float fixedExpense, double exchangeRate, float monthlyNetSalary, String countryCode) {
        super(currencyCode, countryCode, tax, fixedExpense, exchangeRate);
        this.id = id;
        this.monthlyNetSalary = monthlyNetSalary;
    }

    public SalaryDO(CurrencyDO currencyDO, int id, double monthlyNetSalary) {
        super(currencyDO);
        this.id = id;
        this.monthlyNetSalary = monthlyNetSalary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMonthlyNetSalary() {
        return monthlyNetSalary;
    }

    public void setMonthlyNetSalary(double monthlyNetSalary) {
        this.monthlyNetSalary = monthlyNetSalary;
    }

}