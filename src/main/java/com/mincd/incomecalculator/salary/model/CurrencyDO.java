package com.mincd.incomecalculator.salary.model;

/**
 * Domain model used for storing cached exchange rate
 */
public class CurrencyDO {
    private String currencyCode;
    private String countryCode;
    private float tax;
    private float fixedExpense;
    private double exchangeRate;

    public CurrencyDO() {
        //default constructor
    }

    public CurrencyDO(String currencyCode, String countryCode, float tax, float fixedExpense, double exchangeRate) {
        this.currencyCode = currencyCode;
        this.countryCode = countryCode;
        this.tax = tax;
        this.fixedExpense = fixedExpense;
        this.exchangeRate = exchangeRate;
    }

    public CurrencyDO(CurrencyDO currencyDO) {
        this.currencyCode = currencyDO.getCurrencyCode();
        this.countryCode = currencyDO.getCountryCode();
        this.tax = currencyDO.getTax();
        this.fixedExpense = currencyDO.getFixedExpense();
        this.exchangeRate = currencyDO.getExchangeRate();
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public float getFixedExpense() {
        return fixedExpense;
    }

    public void setFixedExpense(float fixedExpense) {
        this.fixedExpense = fixedExpense;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
