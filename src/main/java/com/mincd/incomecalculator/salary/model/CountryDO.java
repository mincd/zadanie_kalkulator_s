package com.mincd.incomecalculator.salary.model;

/**
 * Representing country codes
 */
public class CountryDO {
    private String countryCode;

    public CountryDO(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
