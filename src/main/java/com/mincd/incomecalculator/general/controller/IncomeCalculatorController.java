package com.mincd.incomecalculator.general.controller;

import com.mincd.incomecalculator.exchange.service.ExchangeService;
import com.mincd.incomecalculator.salary.model.CountryDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * General resource used by the whole app
 */
@Controller
public class IncomeCalculatorController {
    @Autowired
    ExchangeService exchangeService;

    /**
     * Get currency codes available in the application
     *
     * @return
     */
    @RequestMapping(value = "/currencies", method = RequestMethod.GET)
    @ResponseBody
    public List<CountryDO> getAvailableCountries() {
        return exchangeService.getAvailableCountries();
    }

}
