package com.mincd.incomecalculator.general.exceptions;

/**
 * Exception for unsupported currencies (eg. currencies that does not exist)
 */
public class UnsupportedCurrencyException extends Exception {
    public UnsupportedCurrencyException(String message) {
        super(message);
    }
}
