package com.mincd.incomecalculator.general.utils;

public class CurrencyUtils {

    private CurrencyUtils() {
        //hiding public constructor
    }

    /**
     * Rounds the passed value to 2 decimal places
     *
     * @param value
     * @return
     */
    public static double roundToTwoDecimals(Double value) {
        return (double) Math.round(value * 100.0) / 100.0;
    }

}