package com.mincd.incomecalculator.salary.service;

import com.mincd.incomecalculator.exchange.service.ExchangeService;
import com.mincd.incomecalculator.general.exceptions.UnsupportedCurrencyException;
import com.mincd.incomecalculator.salary.model.CurrencyDO;
import com.mincd.incomecalculator.salary.model.DailySalaryDO;
import com.mincd.incomecalculator.salary.model.SalaryDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SalaryServiceTest {

    @Mock
    private ExchangeService exchangeService;

    @InjectMocks
    private SalaryService salaryService;

    @Test
    public void calculateIncome() throws UnsupportedCurrencyException {
        //given
        double exchangeRate = 4;
        List<DailySalaryDO> inputSalaries = new ArrayList<>();
        inputSalaries.add(new DailySalaryDO(1, 100d, "DE"));
        CurrencyDO currencyDO = new CurrencyDO("EUR", "DE", 0.2f, 800, exchangeRate);

        //when
        when(exchangeService.getExchangeRateByCountry(any())).thenReturn(currencyDO);

        List<SalaryDO> result = salaryService.calculateIncome(inputSalaries);
        //then

        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(0).getMonthlyNetSalary(), is(equalTo(960d)));
    }

}