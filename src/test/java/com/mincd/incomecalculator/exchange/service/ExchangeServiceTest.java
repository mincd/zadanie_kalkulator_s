package com.mincd.incomecalculator.exchange.service;

import com.mincd.incomecalculator.exchange.config.CachedCurrencies;
import com.mincd.incomecalculator.general.exceptions.UnsupportedCurrencyException;
import com.mincd.incomecalculator.salary.model.CountryDO;
import com.mincd.incomecalculator.salary.model.CurrencyDO;
import org.hamcrest.junit.ExpectedException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;


@RunWith(MockitoJUnitRunner.class)
public class ExchangeServiceTest {

    private static final String SAMPLE_JSON_RESPONSE =
            new StringBuilder(
                    "{\n")
                    .append("    \"table\": \"A\",\n")
                    .append("    \"currency\": \"euro\",\n")
                    .append("    \"code\": \"EUR\",\n")
                    .append("    \"rates\": [\n")
                    .append("        {\n")
                    .append("            \"no\": \"078/A/NBP/2018\",\n")
                    .append("            \"effectiveDate\": \"2018-04-20\",\n")
                    .append("            \"mid\": 4.1724\n")
                    .append("        }\n")
                    .append("    ]\n")
                    .append("}").toString();

    private static final String CURRENCY_CODE_PLN = "PLN";
    private static final String COUNTRY_CODE_PL = "PL";

    @Mock
    private CachedCurrencies cachedCurrencies;

    @InjectMocks
    private ExchangeService exchangeService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private MockRestServiceServer mockServer;

    @Before
    public void prepare() {
        mockServer = MockRestServiceServer.createServer(new RestTemplate());
    }

    /**
     * Test reading of country list
     */
    @Test
    public void getAvailableCountries() {
        //given
        List<CurrencyDO> currencyDOs = new ArrayList<>();
        currencyDOs.add(getSampleCurrency(CURRENCY_CODE_PLN, COUNTRY_CODE_PL));
        currencyDOs.add(getSampleCurrency(CURRENCY_CODE_PLN, COUNTRY_CODE_PL));
        currencyDOs.add(getSampleCurrency(CURRENCY_CODE_PLN, COUNTRY_CODE_PL));

        //when
        when(cachedCurrencies.getCurrencies()).thenReturn(currencyDOs);

        List<CountryDO> result = exchangeService.getAvailableCountries();

        //then
        assertThat(result.size(), is(equalTo(3)));
        assertThat(result.get(0).getCountryCode(), is(equalTo("PL")));

    }

    /**
     * Test reading of exchange rate by country
     */
    @Test
    public void getExchangeRateByCountry() {
        //given
        List<CurrencyDO> currencyDOs = new ArrayList<>();
        currencyDOs.add(getSampleCurrency(CURRENCY_CODE_PLN, COUNTRY_CODE_PL));

        //when
        when(cachedCurrencies.getCurrencies()).thenReturn(currencyDOs);

        CurrencyDO result = null;
        try {
            result = exchangeService.getExchangeRateByCountry("PL");
        } catch (UnsupportedCurrencyException e) {
            assertThat(e, isNull());
        }
        //then
        assertThat(result, notNullValue());
        assertThat(result.getCountryCode(), is(equalTo("PL")));
    }

    /**
     * Test reading of exchange rate with unsupported country
     */
    @Test
    public void getExchangeRateByNotSupportedCountry() throws UnsupportedCurrencyException {
        //given
        List<CurrencyDO> currencyDOs = new ArrayList<>();
        currencyDOs.add(getSampleCurrency(CURRENCY_CODE_PLN, COUNTRY_CODE_PL));

        //when
        when(cachedCurrencies.getCurrencies()).thenReturn(currencyDOs);

        thrown.expect(UnsupportedCurrencyException.class);
        CurrencyDO result = exchangeService.getExchangeRateByCountry("XX");

        //then
        assertThat(result, isNull());
    }

    /**
     * Test exchange rate external request
     */
    @Test
    public void getEurExchangeRate() throws UnsupportedCurrencyException {
        //given
        String currencyCode = "EUR";
        String uri = new StringBuilder(ExchangeService.NBP_EXCHANGE_URI)
                .append(currencyCode).append("?format=json").toString();

        List<CurrencyDO> currencyDOs = new ArrayList<>();
        currencyDOs.add(getSampleCurrency(currencyCode, "DE"));

        //when
        when(cachedCurrencies.getCurrencies()).thenReturn(currencyDOs);
        mockServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(SAMPLE_JSON_RESPONSE, MediaType.APPLICATION_JSON));

        CurrencyDO result = exchangeService.getExchangeRate(currencyCode);

        assertThat(result, notNullValue());
        assertThat(result.getExchangeRate(), is(not(equalTo(0d))));
    }

    private CurrencyDO getSampleCurrency(String currencyCode, String countryCode) {
        return new CurrencyDO(currencyCode, countryCode, 0.19f, 1200, 0d);
    }
}