package com.mincd.incomecalculator.general.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CurrencyUtilsTest {

    @Test
    public void roundToTwoDecimals() {
        //given
        double value = 1.123;

        //when

        double result = CurrencyUtils.roundToTwoDecimals(value);
        //then
        assertThat(result, is(equalTo(1.12)));

    }
}